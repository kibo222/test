package elemenLib;


public class registerPage {
    public static String RegisterURL = "https://accounts.kitabisa.com/register";
    public static String LoginURL = "https://accounts.kitabisa.com/login";
    public static String SnKURL = "https://help.kitabisa.com/articles/360005344814-syarat--ketentuan";
    public static String OtpConfirmURL = "https://accounts.kitabisa.com/register/otp?type=sms";


    public static String Email_Field = "/html/body/div[1]/div[1]/div/div[2]/form/div[1]/div/div/input";
    public static String NamaLengkap_field = "/html/body/div[1]/div[1]/div/div[2]/form/div[2]/div/div/input";
    public static String Daftar_Button = "/html/body/div[1]/div[1]/div/div[2]/form/div[3]/button";
    public static String Ubah_button = "/html/body/div[1]/div[1]/div/div[2]/form/div[1]/div/div/div/span";
    public static String AGREMENT_WA = "/html/body/div[1]/div[1]/div/div[2]/form/div[2]/div/div[2]/p";
    public static String Masuk_link = "/html/body/div[1]/div[1]/div/div[3]/p/a";

    public static String ErrorEmail_Field = "/html/body/div[1]/div[1]/div/div[2]/form/div[1]/div/span";
    public static String ErrorEmailInvalid_Message = "Hanya diisi dengan nomor ponsel atau email yang valid.";
    public static String ErrorEmailblank_Message = "Nomor ponsel atau email tidak boleh kosong.";
    public static String AGREMENT_WA_Message = "Dengan ini bersedia untuk dihubungi via Whatsapp.";

    public static String ErrorName_Field = "/html/body/div[1]/div[1]/div/div[2]/form/div[2]/div/span";
    public static String ErrorName_Message = "Nama lengkap hanya boleh huruf, titik (.) dan apostrof (').";
    public static String ErrorNameBlank_Message = "Nama lengkap tidak boleh kosong.";

    public static String AppleRegister = "/html/body/div[1]/div[2]/div/div[1]/div[1]/button/span";
    public static String GoogleRegister = "/html/body/div[1]/div[2]/div/div[1]/div[2]/button/span";
    public static String FacebookRegister = "/html/body/div[1]/div[2]/div/div[1]/div[3]/button/span";
    public static String SnK = "/html/body/div[1]/div[2]/div/div[2]/a";

    public static String AppleRegister_Text = "Daftar dengan Apple ID";
    public static String GoogleRegister_Text = "Daftar dengan Google";
    public static String FacebookRegister_Text = "Daftar dengan Facebook";


}
