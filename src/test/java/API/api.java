package API;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;



public class api {
    @Test
    public static void getResponse() {

        RestAssured.baseURI = "https://reqres.in";
        RequestSpecification httpRequest = given ();
        Response response = httpRequest.get ( "/api/users?page=2" );
        int statusCode = response.getStatusCode ();
        Assert.assertEquals ( statusCode, 200 );

        if (statusCode == 200)
        {
            System.out.println ( "Correct status code returned" );
        }
        else
        {
            System.out.print ( statusCode );
        }
    }


    @Test
    public  static void create(){

        String data=" '   {  '  + \r\n" +
                " '       \"name\": \"Marlil\",  '  + \r\n" +
                " '       \"job\": \"Test Engineer\"  '  + \r\n" +
                " '  }  ' ; ";

        RestAssured.baseURI="https://reqres.in";

        String Resp=given().
                body("data").
                when().
                post("/api/users").
                then().assertThat().
                statusCode(201).and().
                contentType(ContentType.JSON).and().
                extract().
                response().asString();

        System.out.println("Response is"+Resp);

        String check=given ().
                get ("/api/users").
                then().assertThat().
                statusCode(200).and().
                contentType(ContentType.JSON).and().
                extract().
                response().asString();
        System.out.println("Response is"+check);


    }

}
