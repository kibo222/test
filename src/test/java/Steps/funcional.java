package Steps;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;
import elemenLib.registerPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class funcional {
    @Test
    public static void validatePage(){
        WebDriver driver = new FirefoxDriver ();

        driver.get ( registerPage.RegisterURL );
        WebElement Apple_Button = driver.findElement( By.xpath (registerPage.AppleRegister));
        WebElement Google_Button = driver.findElement( By.xpath (registerPage.GoogleRegister));
        WebElement Facebook_Button = driver.findElement( By.xpath (registerPage.FacebookRegister));
        WebElement SnK_link = driver.findElement( By.xpath (registerPage.SnK));
        String SnKLink = SnK_link.getAttribute ("href"  );

        if (registerPage.AppleRegister_Text.equals ( Apple_Button.getText () ))
        {
            System.out.println ( "Apple Button valid" );
        }

        if (registerPage.GoogleRegister_Text.equals ( Google_Button.getText () ))
        {
            System.out.println ( "Google Button valid" );
        }
        if (registerPage.FacebookRegister_Text.equals ( Facebook_Button.getText () ))
        {
            System.out.println ( "Facebook Button valid" );
        }
        else
         {
             System.out.print ( Apple_Button.getText () + Google_Button.getText () + Facebook_Button.getText () );
             System.out.print ("Please Check with your requirment in PRD or mockup design");
         }


        if (SnKLink.equals ( registerPage.SnKURL ))
        {
            System.out.println ( "Syarat dan Ketentuan link is valid" );

        }
        else
        {
            System.out.println ( "Syarat dan ketentuan tidak sma dengan : " + registerPage.SnKURL );
            System.out.println ( "Tetapi yang di dapatkan adalah = " + SnKLink );
        }


        driver.close();
   }

    @Test
    public static void setBlankEmailField()  {
        WebDriver driver = new FirefoxDriver ();

        driver.get ( registerPage.RegisterURL );
        WebElement email_field = driver.findElement( By.xpath (registerPage.Email_Field));


        email_field.sendKeys ("blabla" );

        WebElement erroremail_field = driver.findElement( By.xpath (registerPage.ErrorEmail_Field));
        String Error_Message = erroremail_field.getText ();

        if (Error_Message.equals ( registerPage.ErrorEmailInvalid_Message ))
        {
            System.out.print ( "cases valid" );
        }
        else {
            System.out.print ( Error_Message );
        }

        WebElement ubah_button  = driver.findElement( By.xpath (registerPage.Ubah_button));
        ubah_button.click ();
        String emailFieldText = email_field.getText ();
        System.out.println ( emailFieldText );

        if (emailFieldText.equals ( "blabla"  ))
        {
            System.out.print ( "error funcion ubah button" );

        }
        else
        {
            System.out.print ( "cases valid" );
        }

    }

    @Test
    public static void checkMasukLinktext (){
        WebDriver driver = new FirefoxDriver ();

        driver.get ( registerPage.RegisterURL );
        WebElement MasukLinkText = driver.findElement( By.xpath (registerPage.Masuk_link));
        MasukLinkText.click ();
        String URL = driver.getCurrentUrl ();

        if (URL.equals ( registerPage.LoginURL ))
        {
            System.out.print ( "cases valid" );
        }
        else
        {
            System.out.print ( "Error login Link direction Page" );
        }
        driver.quit ();
    }

    @Test
    public  static  void WAAgrement()
    {
        WebDriver driver = new FirefoxDriver ();

        driver.get ( registerPage.RegisterURL );
        WebElement Email_field = driver.findElement( By.xpath (registerPage.Email_Field));

        Email_field.sendKeys ("0877289372937" );

        WebElement WATnC = driver.findElement( By.xpath (registerPage.AGREMENT_WA));
        String TnC = WATnC.getText ();
        if (TnC.equals ( registerPage.AGREMENT_WA_Message ))
        {
            System.out.print ( "cases valid" );
        }
        else
        {
            System.out.print ( "WA TnC invalid" + TnC );
        }
        driver.quit ();
    }
}
