package Steps;

import elemenLib.registerPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class negativeCases {

    @Test
    public static void invalidEmail() throws InterruptedException {
        WebDriver driver = new FirefoxDriver ();

        driver.get ( registerPage.RegisterURL );
        WebElement email_field = driver.findElement( By.xpath (registerPage.Email_Field));


        email_field.sendKeys ("blabla" );

        WebElement erroremail_field = driver.findElement( By.xpath (registerPage.ErrorEmail_Field));
        String Error_Message = erroremail_field.getText ();

        if (Error_Message.equals ( registerPage.ErrorEmailInvalid_Message ))
            {
                System.out.print ( "cases valid" );
        }
        else {
            System.out.print ( Error_Message );
        }
        driver.close ();
    }
    @Test
    public static void blankEmail(){
        WebDriver driver = new FirefoxDriver ();

        driver.get ( registerPage.RegisterURL );
        WebElement email_field = driver.findElement( By.xpath (registerPage.Email_Field));

        email_field.sendKeys (" " );

        WebElement erroremail_field = driver.findElement( By.xpath (registerPage.ErrorEmail_Field));
        String Error_Message = erroremail_field.getText ();

        if (Error_Message.equals ( registerPage.ErrorEmailblank_Message ))
        {
            System.out.print ( "cases valid" );
        }
        else {
            System.out.print ( Error_Message );
        }
        driver.close ();

    }
    @Test
    public static void invalidName(){
        WebDriver driver = new FirefoxDriver ();

        driver.get ( registerPage.RegisterURL );
        WebElement email_field = driver.findElement( By.xpath (registerPage.NamaLengkap_field));

        email_field.sendKeys ("434" );

        WebElement erroremail_field = driver.findElement( By.xpath (registerPage.ErrorName_Field));
        String Error_Message = erroremail_field.getText ();

        if (Error_Message.equals ( registerPage.ErrorName_Message ))
        {
            System.out.print ( "cases valid" );
        }
        else {
            System.out.print ( Error_Message );
        }
        driver.close ();

    }
    @Test
    public static void blankName(){
        WebDriver driver = new FirefoxDriver ();

        driver.get ( registerPage.RegisterURL );
        WebElement email_field = driver.findElement( By.xpath (registerPage.NamaLengkap_field));

        email_field.sendKeys (" " );

        WebElement erroremail_field = driver.findElement( By.xpath (registerPage.ErrorName_Field));
        String Error_Message = erroremail_field.getText ();

        if (Error_Message.equals ( registerPage.ErrorNameBlank_Message ))
        {
            System.out.print ( "cases valid" );
        }
        else {
            System.out.print ( Error_Message );
        }
        driver.close ();

    }
}
